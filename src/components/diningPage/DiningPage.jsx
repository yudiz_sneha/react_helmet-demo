import React from 'react';
import Helmet from 'react-helmet';
import './DiningPage.css';

function DiningPage() {
    return (
        <div className='dining-page'>

            {/* HELMET */}
            <Helmet>
                <title>Dining Room Furniture</title>
                <meta name='description' content='Welcome to Dining Room Furnitures' />
            </Helmet>
            
            <div className="container">
                <div className="content">
                    <h2><span>Dining</span> Room Furnitures</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi, illo. Quisquam, ipsam sunt repudiandae vero ullam consequatur commodi aliquam rem dolorem quidem! Amet atque, fugiat dicta deleniti officia quod quibusdam!</p>
                </div>
            </div>
        </div>
    )
}

export default DiningPage;
