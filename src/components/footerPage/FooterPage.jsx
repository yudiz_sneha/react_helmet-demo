import React from 'react';
import { BsFillArrowUpCircleFill } from 'react-icons/bs';
import './FooterPage.css';
import { Link } from 'react-scroll';

function FooterPage() {
    return (
        <div className='footer-page'>
            <div className="container">
                <div className="top">
                    <div className="logo-footer">
                        <h2></h2>
                    </div>
                    <Link activeClass="active" to="top" spy={true} smooth={true} duration={500} >
                        <BsFillArrowUpCircleFill className='icon' />
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default FooterPage;
