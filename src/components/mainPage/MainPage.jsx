import React from 'react';
import Helmet from 'react-helmet';
import './MainPage.css';

function MainPage() {
    return (
        <div className='main-page'>

            {/* HELMET */}
            <Helmet>
                <title>WoodBling Furniture</title>
                <meta name='description' content='Welcome to WoodBling Furniture' />
            </Helmet>

            <div className="container">
                <div className="content">
                    <h1 style={{ color: "white", fontFamily: "cursive" }}>Welcome to <span>WoodBling</span> Furniture</h1>
                </div>
            </div>
        </div>
    )
}

export default MainPage;
