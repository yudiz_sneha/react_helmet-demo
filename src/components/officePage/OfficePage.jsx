import React from 'react';
import './OfficePage.css';

function OfficePage() {
    return (
        <div className='office-page'>
            <div className="container">
                <div className="content">
                <h2 style={{ color: "white" }}><span>Office</span> Furnitures</h2>
                    <p style={{color: "white" }}>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nisi officiis unde perferendis voluptas laborum aliquam culpa officia a maiores porro. Consequuntur officia corporis aliquam numquam, fuga quisquam illo. Obcaecati, voluptatum.</p>
                </div>
            </div>
        </div>
    )
}

export default OfficePage;
