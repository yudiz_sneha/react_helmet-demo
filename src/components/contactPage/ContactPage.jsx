import React from 'react';
import Helmet from 'react-helmet';
import './ContactPage.css';

function ContactPage() {
    return (
        <div className='contact-page'>

            {/* HELMET */}
            <Helmet>
                <title>Contact Us</title>
                <meta name='description' content='For any information contact us. Fill the form' />
            </Helmet>
            
            <div className="container">
                <div className="form-container">
                    <form>
                        <h1><span>Contact</span> Us</h1>
                        <div>
                            <label>Name</label>
                            <input type="text" placeholder='Enter your Name'/>
                        </div>
                        <div>
                            <label>Email</label>
                            <input type="email" placeholder='Enter your Email'/>
                        </div>
                        <div>
                            <label>Message</label>
                            <textarea rows='10' placeholder='Message'/>
                        </div>
                        <button>Submit</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ContactPage;
