import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Dining from './routes/Dining';
import Contact from './routes/Contact';

ReactDOM.render(
  <BrowserRouter>
    <Routes>
      <Route path='/' element={<App />} />
      <Route path='/dining' element={<Dining />} />
      <Route path='/contact' element={<Contact />} />
    </Routes>
  </BrowserRouter>,
  document.getElementById('root')
);