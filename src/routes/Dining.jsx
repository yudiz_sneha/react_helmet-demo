import React from 'react';
import Navbar from '../components/navbar/Navbar';
import DiningPage from '../components/diningPage/DiningPage';

function Dining() {
    return (
        <>
           <Navbar /> 
           <DiningPage />
        </>
    )
}

export default Dining;
