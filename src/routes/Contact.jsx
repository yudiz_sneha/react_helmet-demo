import React from 'react';
import ContactPage from '../components/contactPage/ContactPage';
import Navbar from '../components/navbar/Navbar';

function Contact() {
    return (
        <>
            <Navbar />
            <ContactPage />
        </>
    )
}

export default Contact;
