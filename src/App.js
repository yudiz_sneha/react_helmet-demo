import React from 'react';
import OfficePage from './components/officePage/OfficePage';
import MainPage from './components/mainPage/MainPage';
import Navbar from './components/navbar/Navbar';
import FooterPage from './components/footerPage/FooterPage';
import ReactGA from 'react-ga';

const TRACKING_ID = "UA-226115372-2"; // OUR_TRACKING_ID
ReactGA.initialize(TRACKING_ID);

function App() {
  return (
    <>
      <Navbar />
      <MainPage />
      <OfficePage />
      <FooterPage />
    </>
  );
}

export default App;
